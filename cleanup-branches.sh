#!/bin/zsh
git fetch --prune
BRANCHES=(`git branch --merged | egrep -o '[^\*]*'`)
for branch in $BRANCHES; do
    if [[ $branch != "master" ]]; then
        git branch -d $branch
    fi
done
