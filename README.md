Git Branch Cleaner
==================

A utility script to cleanup old merged git branches
---------------------------------------------------

This script will prune your remote repositories then delete all local branches that have been merged into the current branch.